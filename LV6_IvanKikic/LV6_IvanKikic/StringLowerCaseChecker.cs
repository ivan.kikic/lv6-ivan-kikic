﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV6_IvanKikic
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool containDigit = stringToCheck.Any(char.IsLower);
            return containDigit;
        }
    }
}
