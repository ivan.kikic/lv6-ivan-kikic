﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_IvanKikic
{
    class StringLengthChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Length > 2)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
