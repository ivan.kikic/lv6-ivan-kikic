﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace LV6_IvanKikic
{
    class Program
    {
        static void Main(string[] args)
        {
            // PRVI ZADATAK //
            Notebook knjiga = new Notebook();

            Note note1 = new Note("Prva nota", "Neki tekst za prvu notu");
            Note note2 = new Note("Druga nota", "Neki tekst za drugu notu");
            Note note3 = new Note("Treca nota", "Neki tekst za trecu notu");

            knjiga.AddNote(note1);
            knjiga.AddNote(note2);
            knjiga.AddNote(note3);

            IAbstractIterator iterator = knjiga.GetIterator();
            for(int i = 0; i < 3; i++)
            {
                iterator.Current.Show();
                iterator.Next();
                Console.WriteLine();
            }

            /////////////////////

            // DRUGI ZADATAK //
            Box kutija = new Box();

            Product product1 = new Product("Prvi produkt", 12);
            Product product2 = new Product("Drugi produkt", 1);
            Product product3 = new Product("Treci produkt", 20);

            kutija.AddProduct(product1);
            kutija.AddProduct(product2);
            kutija.AddProduct(product3);

            IAbstractIterator2 iterator2 = kutija.GetIterator2();
            for (int j = 0; j < 3; j++)
            {
                Console.WriteLine(iterator2.Current.ToString());
                iterator2.Next();
                Console.WriteLine();
            }
            //////////////////////

            // TRECI ZADATAK //

            CareTaker cTaker = new CareTaker();
            ToDoItem item = new ToDoItem("Neki naslov", "Neki tekst", DateTime.Today);
            Memento memento = item.StoreState();

            cTaker.AddMemento(memento);
            cTaker.AddMemento(item.StoreState());
            Console.WriteLine(cTaker.GetMemento(0).Title);
            Console.WriteLine(cTaker.GetMemento(1).Title);
            item.Rename("Neki novi naslov");
            Console.WriteLine("// Promjenjen tekst itemu //");
            cTaker.AddMemento(item.StoreState());
            Console.WriteLine(cTaker.GetMemento(2).Title);
            Console.WriteLine();
            int counter = cTaker.Count;
            for (int j = 0; j < counter; j++)
            {
                Console.WriteLine(cTaker.GetMemento(j).Title);
            }
            cTaker.RemoveMemento(memento);
            Console.WriteLine();
            counter = cTaker.Count;
            for (int j = 0; j < counter; j++)
            {
                Console.WriteLine(cTaker.GetMemento(j).Title);
            }
            ///////////////////////

            // CETVRTI ZADATAK //
            Console.WriteLine();
            BankAccount bankRacun = new BankAccount("Ivan Kikic", "Vinkovci", 2000);
            Memento2 memento2 = bankRacun.StoreState();
            Console.WriteLine(memento2.ownerName);
            Console.WriteLine(memento2.ownerAddress);
            Console.WriteLine(memento2.balance);
            Console.WriteLine();
            bankRacun.ChangeOwnerAddress("Osijek");
            bankRacun.UpdateBalance(3000);
            Console.WriteLine(bankRacun.OwnerAddress);
            Console.WriteLine(bankRacun.Balance);
            Console.WriteLine(memento2.ownerAddress);
            Console.WriteLine(memento2.balance);
            ///////////////////////

            // PETI ZADATAK //

            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);
            logger.Log("All", MessageType.ALL);
            logger.Log("Error", MessageType.ERROR);
            logger.Log("Warning", MessageType.WARNING);
            logger.Log("Info", MessageType.INFO);
            
      

        // SESTI ZADATAK //
            string text1 = "Aa1";
            string text2 = "aa1";

            StringChecker checker1 = new StringDigitChecker();
            StringLengthChecker checker2 = new StringLengthChecker();
            StringUpperCaseChecker checker3 = new StringUpperCaseChecker();
            StringLowerCaseChecker checker4 = new StringLowerCaseChecker();

            checker1.SetNext(checker2);
            checker2.SetNext(checker3);
            checker3.SetNext(checker4);

            Console.WriteLine(checker1.Check(text1));
            Console.WriteLine(checker1.Check(text2));


            ////////////////////////

            // SEDMI ZADATAK //

            Console.WriteLine();
            StringChecker passChecker = new StringDigitChecker();
            PasswordValidator validator1 = new PasswordValidator(passChecker);

            validator1.Add(checker2);
            validator1.Add(checker3);
            validator1.Add(checker4);

            validator1.Check(text1);
            validator1.Check(text2);

            ////////////////////////

        }
    }
}
