﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_IvanKikic
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
