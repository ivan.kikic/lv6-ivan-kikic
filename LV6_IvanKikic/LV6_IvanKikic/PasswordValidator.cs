﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_IvanKikic
{
    class PasswordValidator
    {
        StringChecker checkers;
        StringChecker last;
        public PasswordValidator(StringChecker first)
        {
            this.checkers = first;
            this.last = first;
        }

        public void Add(StringChecker checker)
        {
            last.SetNext(checker);
            last = checker;
        }

        public void Check(string tekst)
        {
            Console.WriteLine(checkers.Check(tekst));
        }
    }
}
