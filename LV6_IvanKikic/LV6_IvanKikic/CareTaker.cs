﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_IvanKikic
{
    class CareTaker
    {
        private List<Memento> mementos;
        public CareTaker() { this.mementos = new List<Memento>(); }
        public CareTaker(List<Memento> mementos)
        {
            this.mementos = new List<Memento>(mementos.ToArray());
        }
        public void AddMemento(Memento memento) { this.mementos.Add(memento); }
        public void RemoveMemento(Memento memento) { this.mementos.Remove(memento); }
        public Memento GetMemento(int index) { return this.mementos[index]; }
        public int Count { get { return this.mementos.Count; } }
        public Memento this[int index] { get { return this.mementos[index]; } }
        public Memento PrevoiusState { get; set; }
    }
}
