﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LV6_IvanKikic
{
    class FileLogger : AbstractLogger
    {
        private string filePath;
        public FileLogger(MessageType messageType, string filePath) : base(messageType)
        {
            this.filePath = filePath;
        }
        protected override void WriteMessage(string message, MessageType type)
        {
            using (StreamWriter sw = new StreamWriter(this.filePath, true))
            {
                sw.WriteLine(new string('#', message.Length));
                sw.WriteLine(message);
                sw.WriteLine(new string('#', message.Length));
                sw.WriteLine("Tip: " + type);
                sw.WriteLine("Vrijeme zapisa: " + DateTime.Now);
                Console.WriteLine();
            }
        }
    }
}
