﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV6_IvanKikic
{
    class StringUpperCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool containDigit = stringToCheck.Any(char.IsUpper);
            return containDigit;
        }
    }
}
